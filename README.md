# README #

### What is this repository for? ###

* Start a new webproject with pure css and js without preprocessors? Use this repo to gain easy access to browsersync and a bootstrap ready index file
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* NodeJS/NPM needs to be installed
* after Downloading or cloning this repository you navigate to the root folder and start npm install
* when you start working you have to start the watch tasks with npm run serve
* open the browser and go to http://localhost:3000
* change some files like the index.html or css file and you will see the browser reloading automatically with your changes
* have fun

### Who do I talk to? ###

* Phuc Le
* Code Hard Play Hard Meetup